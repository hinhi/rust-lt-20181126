## RustでシンプルなインメモリKVS作ってみた

中山大樹

2018/11/26 @ RustのLT会！ Rust入門者の集い #6

---

### 自己紹介

+ 名前: 中山大樹
+ Twitter: @WniKwo  github: hinohi gitlab: hinhi（登録時に typo した）
+ 普段は Python、 Rust の前は Fortran/C
+ Rust は プログラミングRust 日本語訳で勉強
    + 発売日(8/8)に買ったので Rust 歴3ヶ月くらいの計算

![プログラミングRust](https://www.oreilly.co.jp/books/images/picture978-4-87311-855-0.gif)

---

### Key Value Store 作ろうと思った経緯

+ Rustは
    + なんか安全らしい
    + とりあえず早いらしい
+ DB 作るしかないな！ |
    + (最初は RDB 作りたかったけど取っ掛かりがわからなかった) |

---

### 作りたかったもののイメージ

+ マルチクライアントで動作するDBサーバー
    + TCPで通信
    + いい感じにロック
+ データの永続化はいらない
    + 生 HashMap
+ クエリはシンプルに
    + Enum

---

### 話すこと

+ データストアのアイデアと実装
+ サーバー化するアイデアと実装

---

### データストア

```rust
pub struct DB {
    div: usize
    dbs: Vec<RwLock<HashMap<String, i64>>>,
}
impl DB {
    pub fn new(div: usize) -> Self {
        let mut dbs = Vec::new();
        for _ in 0..div {
            dbs.push(RwLock::new(HashMap::new()))
        }
        DB { div, dbs }
    }
}
```

+ `std::sync::Mutex` ではなく `std::sync::RwLock`
+ ロック範囲を key のハッシュ値で `div`  個に分割する設計（安易）

---

#### DB に発行するクエリ

```rust
pub enum Query {
    GET(String),
    SET(String, i64),
    ADD(String, i64),
    DELETE(String),
    COUNT,
}
```

+ Enum って便利
+ クエリのパース機能は `FromStr` として実装した
    + `serde` だと実装すべきものが多すぎてしんどかった

---

#### クエリを処理する部分（抜粋）

```rust
impl DB {
    fn do_get(&self, key: String) -> Option<i64> {
        let db = self.dbs[self.key_slot(&key)].read().unwrap();
        db.get(&key).and_then(|v| Some(*v))
    }
    fn do_add(&self, key: String, value: i64) -> Option<i64> {
        let mut db = self.dbs[self.key_slot(&key)].write().unwrap();
        let v = db.entry(key).or_insert(0);
        *v = v.wrapping_add(value);
        Some(*v)
    }
}
```
@[3](`Rwlock`からから読み込み用のロックを獲得)
@[7](`Rwlock`から書き込み用のロックを獲得)
@[4](`Some(&i64)`を`Some(i64)`に簡単に変換したい・・・)
@[9](`wrapping_add`でオーバフローのパニック対策必須。addでパニック起こりがちなの罠だと思う)

---

### サーバー化

+ クライアントとはTCPで会話する
    + Webサーバーと同じ
    + `TcpListener.accept` で待ち構える
    + クライアントが接続してきたら `TcpStream` を取得
    + `TcpStream` を read/write

---

### 素直なサーバーのコード

```rust
pub struct Server {
    addr: String,
    db: Arc<DB>,
}
impl Server {
    pub fn run_forever(&self) -> ! {
        let listener = TcpListener::bind(&self.addr).unwrap();
        loop {
            match listener.accept() {
                Ok((stream, _)) => {
                    let db = Arc::clone(&self.db);
                    thread::spawn(move || worker(db, stream));
                }
                Err(e) => println!("accept error: {}", e),
            }
        }
    }
}
```
@[3,11](`DB`は`Arc`で囲む)
@[12](クラアントが接続してきたタイミングで`thread::spawn`)

---

### 毎回 spawn するの？

+ プレフォークモデルにしたらクライアントが一定数までは最初の応答が早そう（計測してない）
+ スレッドプールは一般的な概念だし Rust でも簡単だよね
   + これ罠

---

### スレッドプール

+ ライブラリはいくつかありそう
    + でもせっかくだから作りたい
    + これに関しては Go みたいな channel で実現したいと思った

---

### channel といえば

+ `std::sync::mpsc`
    + Multi-producer, single-consumer FIFO queue

---

### channel の利用例

```rust
let (tx, rx) = channel();
for i in 0..10 {
    let tx = tx.clone();
    thread::spawn(move|| {
        tx.send(i).unwrap();
    });
}

for _ in 0..10 {
    let j = rx.recv().unwrap();
    assert!(0 <= j && j < 10);
}
```
@[1,3](送信者は clone できる。受信者は `clone` できない)

---

### 送受信器ともに clone できる channel を作った

+ shared_channel
    + https://github.com/hinohi/rust_shared_channel
    + プログラミングRustに載ってたものを実装しきった

```rust
pub struct SharedReceiver<T> {
    inner: Arc<Mutex<Receiver<T>>>,
}
```
@[2](`Arc<Mutex<_>>`のパターン使いやすい)

![プログラミングRust](https://www.oreilly.co.jp/books/images/picture978-4-87311-855-0.gif)

---

### プレフォーク式のサーバー

```rust
pub fn run_forever(&self) -> ! {
    let listener = TcpListener::bind(&self.addr).unwrap();

    let (sender, receiver) = shared_channel();
    for _ in 0..self.num {
        let db = Arc::clone(&self.db);
        let rcv = receiver.clone();
        thread::spawn(move || worker(db, rcv));
    }
    loop {
        match listener.accept() {
            Ok((stream, _)) => sender.send(stream).unwrap(),
            Err(e) => println!("accept error: {}", e),
        }
    }
}
```
@[4](`clone`可能な送受信器ペアの生成)
@[5-9](スレッドプールの立ち上げ)
@[12](`TcpStream`を直接`channel`を通じて送信)

---

### ベンチマーク

+ やりたい

---

### まとめ

+ KVS作った
+ `HashMap` と `RwLock` を使うことで key-value に関しては本当に簡単
+ プレフォーク式のサーバーにしたかったから `channel` を `clone` できるようにした
+ ベンチマークやりたい
